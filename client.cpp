#include "Poco/Net/DatagramSocket.h"
#include <iostream>

int main() {
	Poco::Net::SocketAddress sa("127.0.0.1", 9000);
	Poco::Net::DatagramSocket dgs;
	dgs.connect(sa);
	Poco::Timestamp now;
	std::string msg("Hello, world!");
	dgs.sendBytes(msg.data(), msg.size());
	return 0;
}