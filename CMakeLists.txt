﻿cmake_minimum_required(VERSION 2.8) # Проверка версии CMake.

project(udp)

set(UDP_CPP ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(poco)
include_directories(poco/Net/include)

if(MSVC)
	add_definitions(-DUNICODE -D_UNICODE)
	set(CMAKE_CXX_FLAGS_RELEASE "/MT")
	set(CMAKE_CXX_FLAGS_DEBUG "/MTd /ZI /Od")
else()
	set(CMAKE_CXX_FLAGS_RELEASE "-std=c++11 -fPIC")
	set(CMAKE_CXX_FLAGS_DEBUG "-std=c++11 -fPIC -g -fpermissive")
endif()

set(SOURCE_UDP_SERVER 
	server.cpp)
add_executable(udp_server ${SOURCE_UDP_SERVER})
target_link_libraries(udp_server Net)

set(SOURCE_UDP_CLIENT 
	client.cpp)
add_executable(udp_client ${SOURCE_UDP_CLIENT})
target_link_libraries(udp_client Net)

